import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: [
  ]
})
export class InicioComponent implements OnInit {

  constructor() { }

    // eventos para seleccionar de forma global a los checkbox
    nino: boolean  = false;
    nina: boolean  = false;

  ngOnInit(): void {

    function openNav():any {
      document.getElementById("Medium").style.width = "100%";
    }
    function closeNav():any {
      document.getElementById("Medium").style.width = "0%";
    }

    const image:any = document.querySelector('.zoomimage');
    
    image.addEventListener('mousemove', function (e) {
     let width =  image.offsetWidth;
     let height = image.offsetHeight;
     let mouseX = e.offsetX;
     let mouseY = e.offsetY;      
     let bgPosX = (mouseX / width * 100);
     let bgPosY = (mouseY / height * 100);
     
     image.style.backgroundPosition = `${bgPosX}% ${bgPosY}%`;
    });
    image.addEventListener('mouseleave', function () {
     image.style.backgroundPosition = "center";
    });

    //------------------------------------------------------------------ drowpdown--------------------------------------------------
    document.getElementById('clickable').addEventListener("click", dropDown);

    function dropDown() {
     document.getElementById("myDropdown").classList.toggle("show");
    };
    
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }    
    

  }

  // Checked padre Niño
  onNino(event)
  {
    this.nino = event.target.checked;
 
  }
// Checked padre Niña
  onNina(event)
  {
    this.nina = event.target.checked;
    
  }



}
