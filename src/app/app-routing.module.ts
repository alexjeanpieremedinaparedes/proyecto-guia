import { NgModule } from '@angular/core';
import { RouterModule ,Routes } from '@angular/router'
import { InicioComponent } from './pages/inicio/inicio.component';

const rutas:Routes = [
  {path:'inicio',component:InicioComponent},
  {path:'',pathMatch:'full', redirectTo:'inicio'}
]


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(rutas, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
