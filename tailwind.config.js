const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {

        colors: {
          'azul': {
              '50': '#F0F6FE',
              '100': '#F7F9FD',
              '300': '#1491ED',
              '350': '#148EE0',
              '400': '#0067C2'
          },

          'amarillo': {
            '500': '#FDB819',
            '600': '#E7C126'
          },  

          'plomo': {
              '100': '#F0F0F0',
              '150': '#F5F3F1',
              '180': '#ECECEC',
              '200': '#F6F6F6',
              '250': '#C1C6CA',
              '270': '#5D5D5D',
              '280': '#616161',
              '290': '#424242',
              '300': '#BCBCBC',
              '330': '#959595',
              '400': '#707070',
              '450': '#7A7A7A',
              '500': '#3E3E3E',
              '900': '#262626'
          }
         
        },

        fontFamily: {
          Roboto: ['Roboto'],
          Poppins: ['Poppins'],
          Manrope: ['Manrope']
      }

      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
